import { Component, OnInit } from '@angular/core';

// DECLARO A UNA FUNCION JAVASCRIPT EXISTENTE
declare function init_plugins();

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: []
})
export class PagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // USANDO LA FUNCION JAVASCRIPT
    init_plugins();
  }

}
