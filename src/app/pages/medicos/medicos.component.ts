import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalUploadService } from 'src/app/components/modal-upload/modal-upload.service';
import { Medico } from 'src/app/models/medico.model';
import { MedicoService } from 'src/app/services/medico/medico.service';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: []
})
export class MedicosComponent implements OnInit, OnDestroy {

  public cargando: boolean = true;
  public medicos: Medico[];
  private imgSubs: Subscription;
  public totalRegistros: Number = 0;


  constructor( private medicoService: MedicoService, public _modalUploadService: ModalUploadService) { }
  
  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  ngOnInit() {
    this.cargarMedicos();
    this.imgSubs = this._modalUploadService.notification
      .subscribe( resp => this.cargarMedicos())
  }

  cargarMedicos() {
    this.cargando = true;
    this.medicoService.cargarMedicos().subscribe
      ( (resp: any) => {
        this.cargando = false;
        this.medicos = resp.medicos;
        this.totalRegistros = this.medicos.length;
      })
  }
  mostrarModal( medico: Medico ) {
    this._modalUploadService.mostrarModal('medicos', medico._id, medico.img);
  }

  buscarMedicos( termino: string ) {

    if(termino.length > 0){
      this.medicoService.buscarMedico(termino)
        .subscribe( (medicos: Medico[]) => {
          this.medicos = medicos;
        });
    } else {
      this.cargarMedicos();
    }
  }

  borrarMedico( medico: Medico ){
    
    sweetAlert({
      title: '¿Borrar médico?',
      text: 'Esta a punto de borrar a ' + medico.nombre,
      icon: 'question',
      buttons: ['Cancelar','Aceptar'],
      dangerMode: true
    })
    .then( (borrar) => {
      if( borrar ) {
        this.medicoService.borrarMedico( medico._id )
          .subscribe( borrado => {
            sweetAlert('Médico borrado', medico.nombre + ' ha sido eliminado correctamente', {
              icon: 'success',
            });
            this.cargarMedicos(); 
          });
      }
    })
  }

}
