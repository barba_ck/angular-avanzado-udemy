import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { Hospital } from 'src/app/models/hospital.model';
import { Medico } from 'src/app/models/medico.model';
import { HospitalesService } from 'src/app/services/hospital/hospitales.service';
import { MedicoService } from 'src/app/services/medico/medico.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: []
})
export class MedicoComponent implements OnInit {

  public medicoForm: FormGroup;
  public hospitales: Hospital[];
  public hospitalSeleccionado: Hospital;
  public medicoSeleccionado: Medico;
  

  constructor(
    private fb: FormBuilder,
    private hospitalesService: HospitalesService,
    private medicoService: MedicoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // se que el parametro se llama id porque esta en el routing /medico/:id
    this.activatedRoute.params.subscribe( ({ id }) => {
      this.cargarMedico(id);
    });
    
    this.medicoForm = this.fb.group({
      nombre: ['', Validators.required],
      hospital: ['', Validators.required],
    });
    this.cargarHospitales();
    this.medicoForm.get('hospital').valueChanges
      .subscribe( HospitalId => {
        this.hospitalSeleccionado = this.hospitales.find( hosp => hosp._id === HospitalId);
      })
  }
  cargarMedico(id: string){
    if(id=='nuevo'){
      return;
    }
    this.medicoService.cargarMedicoPorId(id)
      .pipe(
        delay(100)
      )
      .subscribe( (resp:any) => {

        if(!resp.medico) {
          this.router.navigateByUrl(`/404`);
        }
        const { nombre, hospital: { _id }} = resp.medico;
        this.medicoSeleccionado = resp.medico;
        this.medicoForm.setValue({ nombre, hospital: _id });
      });
  }

  cargarHospitales() {
    this.hospitalesService.cargarHospitales()
      .subscribe( (resp: any) => {
        this.hospitales = resp.hospitales;
      })
  }

  guardarMedico() {
    if (this.medicoSeleccionado){
      const data = {
        ...this.medicoForm.value,
        _id: this.medicoSeleccionado._id
      }
      this.medicoService.actualizarMedico(data)
      .subscribe( resp => {
        this.router.navigateByUrl(`/medicos/${ resp.medico._id }`);
      });
    } else {
      this.medicoService.crearMedico( this.medicoForm.value)
      .subscribe( resp => {
        this.router.navigateByUrl(`/medicos/${ resp.medico._id }`);
      });
    }
  }

}
