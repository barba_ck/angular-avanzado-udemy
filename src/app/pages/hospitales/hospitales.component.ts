import { Component, OnDestroy, OnInit } from '@angular/core';
import { Hospital } from 'src/app/models/hospital.model';
import { HospitalesService } from 'src/app/services/hospital/hospitales.service';
import { ModalUploadService } from 'src/app/components/modal-upload/modal-upload.service';
import { Subscription } from 'rxjs';

declare var swal: any;

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styleUrls: []
})
export class HospitalesComponent implements OnInit, OnDestroy {

  hospitales: Hospital[] = [];
  desde: number = 0;
  totalRegistros: number = 0;
  cargando: boolean = true;
  private imgSubs: Subscription;

  constructor(
    public _hospitalesService: HospitalesService,
    public _modalUploadService: ModalUploadService
  ) { }

  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  ngOnInit() {
    this.cargarHospitales();

    this.imgSubs = this._modalUploadService.notification
      .subscribe( resp => this.cargarHospitales())
  }
  
  cargarHospitales() {
    this.cargando = true;

    this._hospitalesService.cargarHospitales(this.desde)
      .subscribe( (resp: any) => {
        this.cargando = false;
        this.totalRegistros = resp.total;
        this.hospitales = resp.hospitales;
      });
  }

  cambiarDesde(valor: number) {
    let desde = this.desde + valor;

    if( desde >= this.totalRegistros ) {
      return;
    }
    if( desde < 0 ) {
      return;
    }
    this.desde = desde;
    this.cargarHospitales();
  }

  buscarHospitales( termino: string ) {

    if(termino.length > 0){
      this._hospitalesService.buscarHospital(termino)
        .subscribe( (hospitales: Hospital[]) => {
          this.hospitales = hospitales;
        });
    } else {
      this.cargarHospitales();
    }
  }

  crearHospital() {
    swal({
      title: 'Crear hospital',
      text: 'Ingrese el nombre del hospital.',
      content: "input",
      button: {
        text: "Crear",
        closeModal: true,
      },
    })
    .then(name => {
      if (!name) throw null;
     
      return this._hospitalesService.crearHospital(name)
        .subscribe(( resp: any ) => {
          this.cargarHospitales();
        });
    })
    .catch(err => {
      if (err) {
        swal("Error!", "Falló la creación del hospital", "error");
      } else {
        swal.stopLoading();
        swal.close();
      }
    });
  }

  guardarHospital( hospital: Hospital ){
    this._hospitalesService.actualizarHospital(hospital)
        .subscribe(( resp: any ) => {
          swal("Correcto", "Hospital actualizado");
          this.cargarHospitales();
        }, error => {
          swal("Error!", "Ha ocurrido un error", "error");
          console.error(error);
        });
  }
  
  borrarHospital( hospital: Hospital ){
    this._hospitalesService.borrarHospital(hospital._id)
        .subscribe(() => {
          swal("Correcto", "Hospital borrado con éxito");
          this.cargarHospitales();
        }, error => {
          swal("Error!", "Ha ocurrido un error", "error");
          console.error(error);
        });
  }

  mostrarModal( hospital: Hospital ) {
    this._modalUploadService.mostrarModal('hospitales', hospital._id, hospital.img);
  }

}
