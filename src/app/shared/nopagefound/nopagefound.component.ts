import { Component, OnInit } from '@angular/core';

// DECLARO A UNA FUNCION JAVASCRIPT EXISTENTE
declare function init_plugins();

@Component({
  selector: 'app-nopagefound',
  templateUrl: './nopagefound.component.html',
  styles: []
})
export class NopagefoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // USANDO LA FUNCION JAVASCRIPT
    init_plugins();
  }

}
