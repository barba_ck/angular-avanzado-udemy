import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { URL_SERVICIOS } from 'src/app/config/config';
import { Medico } from 'src/app/models/medico.model';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  constructor( 
    private http: HttpClient,
    public usuarioService: UsuarioService
  ) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }
  get headers() {
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  cargarMedicos(desde: number = 0) {

    let url = URL_SERVICIOS + '/medico?desde=' + desde;
    return this.http.get( url );

  }
  cargarMedicoPorId(id: String) {

    let url = URL_SERVICIOS + '/medico/' + id;
    return this.http.get( url );

  }

  obtenerHospital( id: string ) {
    let url = URL_SERVICIOS + '/hospital/' +id;
    return this.http.get( url );
  }

  crearMedico( medico: { nombre: string, hospital: string } ){
    let url = URL_SERVICIOS + '/medico';
    url += '?token=' + this.token;

    return this.http.post(url, medico).pipe(
      map((resp: any) => {
        sweetAlert('Médico creado', medico.nombre, 'success');
        return resp.medico;
    }));
  }

  actualizarMedico( medico: Medico ) {
    let url = URL_SERVICIOS + '/medico/' + medico._id;
    url += '?token=' + this.token;

    return this.http.put( url, medico ).pipe(
      map( (resp: any) => {
        if(resp.ok){
          sweetAlert('Médico actualizado', resp.medico.nombre, 'success');
        }
        return resp;
      }));
  } 

  borrarMedico( id: string ){
    let url = URL_SERVICIOS + '/medico/' +id;
    url += '?token=' + this.token;
    return this.http.delete( url ).pipe(
      map((resp: any) => {
        if(resp.ok){
          sweetAlert('Médico borrado', resp.medico.nombre, 'success');
        }
        return resp;
    }));
    
  }

  buscarMedico( termino: string) {
    let url = URL_SERVICIOS + '/busqueda/coleccion/medicos/' + termino;

    return this.http.get( url ).pipe(
      map( (resp: any ) => resp.medicos)
    );
  }

}
