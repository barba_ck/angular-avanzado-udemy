import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import swal from 'sweetAlert';
import { Hospital } from 'src/app/models/hospital.model';
import { URL_SERVICIOS } from 'src/app/config/config';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class HospitalesService {

  constructor(
    public http: HttpClient,
    public usuarioService: UsuarioService
  ) {
    // this.token = localStorage.getItem('token');
  }

  get token(): string {
    return localStorage.getItem('token') || '';
  }
  get headers() {
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  cargarHospitales(desde: number = 0) {

    let url = URL_SERVICIOS + '/hospital?desde=' + desde;
    return this.http.get( url );

    // let url = URL_SERVICIOS + '/hospital?desde=' + desde;
    // return this.http.get( url, this.headers )
    //   .pipe(
    //     map( (resp: {ok: boolean, hospitales: Hospital[] }) => resp.hospitales)
    //   );
  }

  obtenerHospital( id: string ) {
    let url = URL_SERVICIOS + '/hospital/' +id;
    return this.http.get( url );
  }

  borrarHospital( id: string ){
    let url = URL_SERVICIOS + '/hospital/' +id;
    url += '?token=' + this.usuarioService.token;
    return this.http.delete( url ).pipe(
      map((resp: any) => {
        if(resp.ok){
          swal('Hospital borrado', resp.hospital.nombre, 'success');
        }
        return resp;
    }));
    
  }

  crearHospital( nombre: string ){
    let url = URL_SERVICIOS + '/hospital';
    url += '?token=' + this.usuarioService.token;
    let hospital = new Hospital(nombre, null, null);

    return this.http.post(url, hospital).pipe(
      map((resp: any) => {
        swal('Hospital creado', hospital.nombre, 'success');
        return resp.hospital;
    }));
  }

  buscarHospital( termino: string) {
    let url = URL_SERVICIOS + '/busqueda/coleccion/hospitales/' + termino;

    return this.http.get( url ).pipe(
      map( (resp: any ) => resp.hospitales)
    );
  }

  actualizarHospital( hospital: Hospital ) {
    let url = URL_SERVICIOS + '/hospital/' + hospital._id;
    url += '?token=' + this.usuarioService.token;

    return this.http.put( url, hospital ).pipe(
      map( (resp: any) => {
        console.log(resp);
        if(resp.ok){
          swal('Hospital actualizado', resp.hospital.nombre, 'success');
        }
        return resp;
      }));
  } 
}
