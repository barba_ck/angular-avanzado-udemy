import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { UsuarioService } from './usuario/usuario.service';

import {
  SettingsService,
  SharedService,
  SidebarService
} from './service.index';
import { ModalUploadService } from '../components/modal-upload/modal-upload.service';
import { HospitalesService } from './hospital/hospitales.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    SettingsService,
    SharedService,
    SidebarService,
    UsuarioService,
    ModalUploadService,
    HospitalesService,
  ]
})
export class ServiceModule { }
