import { Injectable, EventEmitter } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/config/config';

@Injectable({
  providedIn: 'root'
})
export class ModalUploadService {

  public tipo: string;
  public id: string;
  public oculto: string = 'oculto';
  public imagenTemp: string = '';
  

  public notification = new EventEmitter<any>();

  constructor() { }

  mostrarModal(tipo: string, id: string, img: string) {
    let url = URL_SERVICIOS + '/img';
    this.id = id;
    this.tipo = tipo;
    this.oculto = '';
    this.imagenTemp = `${url}/${tipo}/${img}`;
  }

  ocultarModal() {
    this.id = null;
    this.tipo = null;
    this.oculto = 'oculto';
  }

  
}
