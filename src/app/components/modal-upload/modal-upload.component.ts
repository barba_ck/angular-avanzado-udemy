import { Component, OnInit } from '@angular/core';
import { SubirArchivoService } from 'src/app/services/service.index';
import { ModalUploadService } from './modal-upload.service';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {

  imagenSubir: File;
  imagenTemp: string | ArrayBuffer;

  constructor(
    public _subirArchivoService: SubirArchivoService,
    public _modalUploadService: ModalUploadService
  ) { }

  ngOnInit() {
    this.imagenTemp = this._modalUploadService.imagenTemp;
  }

  seleccionImagen( archivo: File ) {

    if(!archivo){
      return;
    } 

    if(archivo.type.indexOf('image') < 0){
      sweetAlert('Sólo imagenes', 'El archivo seleccionado no es una imágen', 'error');
      this.imagenSubir = null;
      return ;
    }

    this.imagenSubir = archivo;

    let reader = new FileReader();
    let urlImagenTemp = reader.readAsDataURL( archivo );

    reader.onloadend = () => this.imagenTemp = reader.result;
  }

  cerrarModal(): void {
    this.imagenSubir = null;
    this.imagenTemp = null;

    this._modalUploadService.ocultarModal();
  }

  subirImagen(){

    this._subirArchivoService.subirArchivo(this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id)
      .then( resp => {

        this._modalUploadService.notification.emit( resp );
        this.cerrarModal();
      })
      .catch( error => {
        console.log('Error en la carga. ', error);
      })
  }

}
